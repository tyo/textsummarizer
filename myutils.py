import os
sampleDir = "articles"
outputExt = "_out"
sample1 = "reuters_toyota_sample.txt"
sample2 = "reuters_walmart_sample.txt"
sampleUrl1 = "https://pastebin.com/raw/S4QV4kFK"
sampleUrl2 = "https://pastebin.com/raw/bXemuL3f"


def printToFile(outFile,summary): 
    with open(outFile, encoding='utf-8', mode='w+') as file: 
        for sentence in summary: 
            file.write(sentence)

def prettyPrintSentenceList(sentences):
    for sentence in sentences:
        print(sentence)

def makeOutputDirectory(directory):
    outdir = directory+outputExt
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    return outdir

def getFilesFromDirectory(directory):
    for file in os.listdir(directory):
        if file[0] is '.':
            continue
        else : 
            yield file


