from nltk.tokenize import punkt
from nltk.corpus import reuters

class SentenceTokenizer:

    def __init__(self):
        self._pt = punkt.PunktTrainer()
        self._pst = punkt.PunktSentenceTokenizer()
        #hack to avoid newing up more PSTs later
        self._pst._params = self._pt._params    

    def initialTraining(self, rawText):
        self._pt.train(rawText, finalize=False)

    def finalizeInitialTraining(self):
        self._pt.finalize_training()

    def trainOnText(self,rawText):
        self._pt.train(rawText)

    def tokenize(self, rawText):
        return self._pst.tokenize(rawText)
    

sentenceTokenizer = SentenceTokenizer()
for article in reuters.fileids('jobs'):
    sentenceTokenizer.initialTraining(reuters.raw(article))

sentenceTokenizer.finalizeInitialTraining()
