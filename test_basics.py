import basics
from nltk.corpus import stopwords

def test_filterStopwords():
    stopSet = set(stopwords.words("english"))
    testWordSet = set(["Jabberwocky", "plink"])
    testSet = stopSet.union(testWordSet)
    assert set(basics.filterTokens(testSet)) == testWordSet

def test_filterPunctuation():
    testWordSet = set(["Jabberwocky", "plink"])
    hitSet = set(["'", "!", "?"])
    testSet = testWordSet.union(hitSet)
    assert set(basics.filterTokens(testSet)) == testWordSet

sentences = ["My name is Bob.", "I live in a house.", "The house is green."]

def test_wordCount():
    sc = {"name":1, "bob":1, "live":1, "house":2, "green":1}
    wordCount = basics.getWordCountFromSentences(sentences)
    assert len(sc) == len(wordCount)
    for key,value in wordCount.items():
        assert sc[key] == value

def test_createSummary():
    sentenceScores = {sentences[0]:1, sentences[1]:2, sentences[2]:3}
    summaryLength = 2
    sumSentences, summary = basics.createSummary(
        sentences, sentenceScores, summaryLength)
    assert summary  == sentences[1:]


