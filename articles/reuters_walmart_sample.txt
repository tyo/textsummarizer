McMillon's comments capped a three-day media event during which the company announced initiatives like a test program that allows store workers to deliver packages ordered on the retailer's website and the use of blockchain technology to ensure safety in its food supply chain.

Wal-Mart's investments in technology helped deliver a 63 percent rise in first quarter online sales, up from 29 percent growth in the fourth quarter and 20 percent in the third quarter.

Its results have also outshined rivals like Target Corp (TGT.N), department-store chains and apparel retailers.

The shareholders meeting at the Bud Walton Arena in Fayetteville, Arkansas, 30 miles from the company's headquarters in Bentonville, was packed with 14,000 people, including workers from around 27 countries and shareholders. 
The company tapped Blake Shelton to host the show which included pop stars like Gwen Stefani, Mary J. Blige and Rachel Platten.

Bitcoin exchange Coinbase seeks new funds at $1 billion valuation: Wall Street Journal
Wal-Mart's shareholders approved all 11 company-recommended board members and voted in-line with company wishes.
None of the four shareholder proposals put forward were approved.
One from Making Change at Wal-Mart, part of the United Food and Commercial Workers Union, called for an independent chairman to act in the interests of Wal-Mart's hourly workers.

"How can any Wal-Mart associate build a better life when they have been with the company for five years or more and they make $9 or $10 an hour," said Amy Ritter from the labor group, who presented the proposal.

For years, influential proxy advisory firms have recommended shareholders vote in favor of an independent board chairman without ties to management or the founding family, ever since bribery allegations several years ago exposed serious board oversight failures.

About 51 percent of Wal-Mart's stock is controlled by the Walton family and current Chairman of the Board Greg Penner is married to the grandaughter of Wal-Mart founder Sam Walton.