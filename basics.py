from nltk import word_tokenize
from nltk.corpus import stopwords
from collections import defaultdict
import string
import re
import sentok
from myutils import *

st = sentok.sentenceTokenizer

def getRawText(document):
    with open(document, encoding='utf-8') as f:
        raw = f.read()
        return raw
	
def getSentences(rawText):
    return st.tokenize(rawText)

def getTokens(rawText):
    return word_tokenize(rawText)

def getWordCountFromSentences(sentences):
    wordCount = defaultdict(int)
    for sentence in sentences:
        for token in filterTokens(getTokens(sentence.lower())):
            wordCount[token] += 1
    return wordCount
	
def filterTokens(tokens, stopwordSet=set(stopwords.words("english"))):
    expr = "^["+string.punctuation+"]+$"#|.*["+"'"+"]+.*"
    tokens = [w for w in tokens if w not in stopwordSet]
    tokens = [w for w in tokens if re.match(expr,w) is None]
    return tokens

def getSentenceScores(sentences):
    wordCount = getWordCountFromSentences(sentences)
    sentenceValues = defaultdict(int)	
    for sentence in sentences:
        #TODO: get rid of this double tokenizing.
        for word in getTokens(sentence):
            wordValue = wordCount.get(word)
            if wordValue is not None:
                sentenceValues[sentence]+=wordValue
    return sentenceValues
    
def createSummary(sentences, sentenceScores, summaryLength):
    topScores = sorted(
        [s[1] for s in sentenceScores.items()], reverse=True)[:summaryLength]
    summary = []
    for sentence in sentences:
        if not len(topScores):
            break
        score = sentenceScores[sentence]
        if score in topScores:
            summary.append(sentence)
            topScores.remove(score)
    return sentences, summary
    
def processText(rawText, summaryLength=5):
    st.trainOnText(rawText)
    sentences = getSentences(rawText)
    sentenceScores = getSentenceScores(sentences)
    return createSummary(sentences, sentenceScores, summaryLength)

def processDirectory(directory):
    outdir = makeOutputDirectory(directory)
    for file in getFilesFromDirectory(directory):
        outfile = outdir+"/"+file
        infile = directory+"/"+file
        sentences, summary = processText(getRawText(infile))
        print("=="+file+"==")
        prettyPrintSentenceList(sentences)
        print('==Summary==')
        prettyPrintSentenceList(summary)
        print("==Done==")
        printToFile(outfile, summary)

#Convenience
def runSampleDir():
    processDirectory(sampleDir)
